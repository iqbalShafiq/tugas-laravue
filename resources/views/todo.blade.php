@extends('layouts.master')

@section('title')
Todo App
@endsection

@section('content')
<div id="app">
    <h1>All My Todos</h1>
    <input type="text" name="todo" id="todo" v-model="newTodo" class="mt-2 px-2 text-center"
        placeholder="Your todo here ..." @keyup.enter="createTodo">
    <ul class="list-group list-group-flush" v-for="(todo, index) in todos">
        <li class="list-group-item">
            <span v-bind:class="{'completed': todo.done}">
                @{{ todo.name }}
            </span>
            <div class="mt-2">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-danger mr-2" data-toggle="modal"
                    data-target="#exampleModal">Delete</button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
                            </div>
                            <div class="modal-body">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal"
                                    v-on:click="deleteTodo(index, todo)">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-warning" v-on:click="updateTodo(todo)">Done</button>
            </div>
        </li>
    </ul>
</div>
@endsection

@push('script')
<script>
    new Vue({
        el: "#app",
        data: {
            newTodo: "",
            todos: []
        },
        methods: {
            createTodo: function() {
                let inputText = this.newTodo.trim();
                this.$http.post('/api/todo/create', {name: inputText}).then(response => {
                    this.todos.unshift({
                        name: inputText,
                        done: 0
                    });
                    this.newTodo = "";
                });
            },
            updateTodo: function(todo) {
                this.$http.patch('/api/todo/update/' + todo.id).then(response => {
                    todo.done = !todo.done;
                });
            },
            deleteTodo: function(index, todo) {
                this.$http.delete('/api/todo/delete/' + todo.id).then(response => {
                    this.todos.splice(index, 1);
                });
            }
        },
        mounted: function() {
            this.$http.get('/api/todo/read').then(response => {
                let result = response.body;
                this.todos = result;
            });
        }
    });
</script>
@endpush
