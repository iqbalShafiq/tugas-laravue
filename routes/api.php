<?php

Route::namespace('Todo')->group(function () {
    Route::post('todo/create', 'CreateController');
    Route::get('todo/read', 'ReadController');
    Route::patch('todo/update/{todo}', 'UpdateController');
    Route::delete('todo/delete/{todo}', 'DeleteController');
});
