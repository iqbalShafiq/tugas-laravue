<?php
Route::get('/', function () {
    return view('welcome');
});

Route::get('/my-todo', function () {
    return view('todo');
});
