<?php

namespace App\Http\Controllers\Todo;

use App\Http\Controllers\Controller;
use App\Todo;
use Illuminate\Http\Request;

class DeleteController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, Todo $todo)
    {
        Todo::where('id', $todo->id)->delete();

        return 'deleted';
    }
}
