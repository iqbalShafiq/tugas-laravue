<?php

namespace App\Http\Controllers\Todo;

use App\Http\Controllers\Controller;
use App\Todo;
use Illuminate\Http\Request;

class UpdateController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, Todo $todo)
    {
        $toggleDone = $todo->done === 0 ? 1 : 0;
        Todo::where('id', $todo->id)->update([
            'done' => $toggleDone
        ]);

        return 'updated!';
    }
}
