<?php

namespace App\Http\Controllers\Todo;

use App\Http\Controllers\Controller;
use App\Todo;
use Illuminate\Http\Request;

class CreateController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $todo = Todo::create([
            'name' => request('name'),
        ]);

        return $todo;
    }
}
