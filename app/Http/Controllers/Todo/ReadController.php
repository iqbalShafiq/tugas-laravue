<?php

namespace App\Http\Controllers\Todo;

use App\Http\Controllers\Controller;
use App\Todo;
use Illuminate\Http\Request;

class ReadController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $todos = Todo::orderBy('created_at', 'desc')->get();

        return $todos;
    }
}
